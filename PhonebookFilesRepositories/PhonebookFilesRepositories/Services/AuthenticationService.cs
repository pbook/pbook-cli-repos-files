﻿using PhonebookFilesRepositories.Entities;
using PhonebookFilesRepositories.Repositories;

namespace PhonebookFilesRepositories.Services
{
    class AuthenticationService
    {
        public static User LoggedUser { get; private set; }

        public static void AuthenticateUser(string username, string password)
        {
            UserRepository userRepo = new UserRepository(@"../../DataFiles/users.txt");
            AuthenticationService.LoggedUser = userRepo.GetByUsernameAndPassword(username, password);
        }
    }
}
