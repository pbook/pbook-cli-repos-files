﻿namespace PhonebookFilesRepositories.Tools
{
    public enum UserMenu
    {
        Insert = 1,
        Delete = 2,
        Update = 3,
        Select = 4,
        View = 5,
        Exit = 6
    }

    public enum ContactMenu
    {
        Insert = 1,
        Delete = 2,
        Update = 3,
        Select = 4,
        View = 5,
        Exit = 6
    }

    public enum PhoneMenu
    {
        Insert = 1,
        Delete = 2,
        Update = 3,
        Select = 4,
        Exit = 5
    }
}
