﻿namespace PhonebookFilesRepositories.Entities
{
    class Phone : BaseEntity
    {
        public int ParentContactId { get; set; }
        public string PhoneNumber { get; set; }
    }
}
