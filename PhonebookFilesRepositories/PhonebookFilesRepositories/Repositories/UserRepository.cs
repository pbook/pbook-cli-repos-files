﻿using PhonebookFilesRepositories.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace PhonebookFilesRepositories.Repositories
{
    class UserRepository : BaseRepository<User>
    {
        public UserRepository(string filePath) : base(filePath)
        {
        }

        public User GetByUsernameAndPassword(string username, string password)
        {
            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    User entity = new User();
                    entity.Id = Convert.ToInt32(sr.ReadLine());
                    entity.FirstName = sr.ReadLine();
                    entity.LastName = sr.ReadLine();
                    entity.Username = sr.ReadLine();
                    entity.Password = sr.ReadLine();
                    entity.IsAdmin = Convert.ToBoolean(sr.ReadLine());

                    if (entity.Username == username && entity.Password == password)
                    {
                        return entity;
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return null;
        }

        protected override void ReadEntity(User entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.FirstName = sr.ReadLine();
            entity.LastName = sr.ReadLine();
            entity.Username = sr.ReadLine();
            entity.Password = sr.ReadLine();
            entity.IsAdmin = Convert.ToBoolean(sr.ReadLine());
        }

        protected override void WriteEntity(User entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.FirstName);
            sw.WriteLine(entity.LastName);
            sw.WriteLine(entity.Username);
            sw.WriteLine(entity.Password);
            sw.WriteLine(entity.IsAdmin);
        }
    }
}
