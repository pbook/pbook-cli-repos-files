﻿using PhonebookFilesRepositories.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace PhonebookFilesRepositories.Repositories
{
    class PhoneRepository : BaseRepository<Phone>
    {
        public PhoneRepository(string filePath) : base(filePath)
        {
        }

        public List<Phone> GetAll(int parentContactId)
        {
            List<Phone> result = new List<Phone>();

            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    Phone phone = new Phone();
                    phone.Id = Convert.ToInt32(sr.ReadLine());
                    phone.ParentContactId = Convert.ToInt32(sr.ReadLine());
                    phone.PhoneNumber = sr.ReadLine();

                    if (phone.ParentContactId == parentContactId)
                    {
                        result.Add(phone);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        protected override void ReadEntity(Phone entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.ParentContactId = Convert.ToInt32(sr.ReadLine());
            entity.PhoneNumber = sr.ReadLine();
        }

        protected override void WriteEntity(Phone entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.ParentContactId);
            sw.WriteLine(entity.PhoneNumber);
        }
    }
}
