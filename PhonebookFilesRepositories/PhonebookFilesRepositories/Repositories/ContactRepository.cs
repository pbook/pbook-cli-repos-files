﻿using PhonebookFilesRepositories.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace PhonebookFilesRepositories.Repositories
{
    class ContactRepository : BaseRepository<Contact>
    {

        public ContactRepository(string filePath) : base(filePath)
        {
        }

        public List<Contact> GetAll(int parentUserId)
        {
            List<Contact> result = new List<Contact>();

            FileStream fs = new FileStream(this.filePath, FileMode.OpenOrCreate);
            StreamReader sr = new StreamReader(fs);

            try
            {
                while (!sr.EndOfStream)
                {
                    Contact entity = new Contact();
                    entity.Id = Convert.ToInt32(sr.ReadLine());
                    entity.ParentUserId = Convert.ToInt32(sr.ReadLine());
                    entity.FullName = sr.ReadLine();
                    entity.Email = sr.ReadLine();

                    if (entity.ParentUserId == parentUserId)
                    {
                        result.Add(entity);
                    }
                }
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return result;
        }

        protected override void ReadEntity(Contact entity, StreamReader sr)
        {
            entity.Id = Convert.ToInt32(sr.ReadLine());
            entity.ParentUserId = Convert.ToInt32(sr.ReadLine());
            entity.FullName = sr.ReadLine();
            entity.Email = sr.ReadLine();
        }

        protected override void WriteEntity(Contact entity, StreamWriter sw)
        {
            sw.WriteLine(entity.Id);
            sw.WriteLine(entity.ParentUserId);
            sw.WriteLine(entity.FullName);
            sw.WriteLine(entity.Email);
        }
    }
}
