﻿using PhonebookFilesRepositories.Entities;
using PhonebookFilesRepositories.Repositories;
using PhonebookFilesRepositories.Tools;
using System;
using System.Collections.Generic;

namespace PhonebookFilesRepositories.Views
{
    class PhoneView
    {
        private Contact contact = null;

        public PhoneView(Contact contact)
        {
            this.contact = contact;
        }

        public void Show()
        {
            while (true)
            {
                PhoneMenu choice = RenderMenu();

                switch (choice)
                {
                    case PhoneMenu.Select:
                        {
                            GetAll();
                            break;
                        }
                    case PhoneMenu.Insert:
                        {
                            Add();
                            break;
                        }
                    case PhoneMenu.Delete:
                        {
                            Delete();
                            break;
                        }
                    case PhoneMenu.Exit:
                        {
                            return;
                        }
                }
            }
        }

        private PhoneMenu RenderMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Phones management (" + this.contact.FullName + ")");
                Console.WriteLine("ID:" + contact.Id);
                Console.WriteLine("Name :" + contact.FullName);
                Console.WriteLine("Email :" + contact.Email);
                Console.WriteLine("##########################");
                Console.WriteLine("[G]et all Phones");
                Console.WriteLine("[A]dd Phone");
                Console.WriteLine("[D]elete Phone");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "G":
                        {
                            return PhoneMenu.Select;
                        }
                    case "A":
                        {
                            return PhoneMenu.Insert;
                        }
                    case "D":
                        {
                            return PhoneMenu.Delete;
                        }
                    case "X":
                        {
                            return PhoneMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        private void GetAll()
        {
            Console.Clear();

            PhoneRepository phonesRepository = new PhoneRepository(@"../../DataFiles/phones.txt");
            List<Phone> phones = phonesRepository.GetAll(this.contact.Id);

            foreach (Phone phone in phones)
            {
                Console.WriteLine("ID:" + phone.Id);
                Console.WriteLine("Phone :" + phone.PhoneNumber);
                Console.WriteLine("########################################");
            }

            Console.ReadKey(true);
        }

        private void Add()
        {
            Console.Clear();

            Phone phone = new Phone();
            phone.ParentContactId = this.contact.Id;

            Console.WriteLine("Add new Phone:");
            Console.Write("Phone: ");
            phone.PhoneNumber = Console.ReadLine();

            PhoneRepository phonesRepository = new PhoneRepository(@"../../DataFiles/phones.txt");
            phonesRepository.Save(phone);

            Console.WriteLine("Phone saved successfully.");
            Console.ReadKey(true);
        }

        private void Delete()
        {
            PhoneRepository phonesRepository = new PhoneRepository(@"../../DataFiles/phones.txt");

            Console.Clear();

            Console.WriteLine("Delete Phone:");
            Console.Write("Phone Id: ");
            int phoneId = Convert.ToInt32(Console.ReadLine());

            Phone phone = phonesRepository.GetById(phoneId);
            if (phone == null)
            {
                Console.WriteLine("Phone not found!");
            }
            else
            {
                phonesRepository.Delete(phone);
                Console.WriteLine("Phone deleted successfully.");
            }
            Console.ReadKey(true);
        }
    }
}
