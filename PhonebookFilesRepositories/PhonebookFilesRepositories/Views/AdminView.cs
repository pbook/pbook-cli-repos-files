﻿using System;

namespace PhonebookFilesRepositories.Views
{
    public class AdminView
    {
        public void Show()
        {
            while (true)
            {
                Console.Clear();

                while (true)
                {
                    Console.Clear();
                    Console.WriteLine("Administration View:");
                    Console.WriteLine("[U]ser Management");
                    Console.WriteLine("[C]ontacts Management");
                    Console.WriteLine("E[x]it");

                    string choice = Console.ReadLine();
                    switch (choice.ToUpper())
                    {
                        case "U":
                            {
                                UserView view = new UserView();
                                view.Show();

                                break;
                            }
                        case "C":
                            {
                                ContactView view = new ContactView();
                                view.Show();

                                break;
                            }
                        case "X":
                            {
                                return;
                            }
                        default:
                            {
                                Console.WriteLine("Invalid choice.");
                                Console.ReadKey(true);
                                break;
                            }
                    }
                }
            }
        }
    }
}
