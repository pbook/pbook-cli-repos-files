﻿using PhonebookFilesRepositories.Entities;
using PhonebookFilesRepositories.Repositories;
using PhonebookFilesRepositories.Services;
using PhonebookFilesRepositories.Tools;
using System;
using System.Collections.Generic;

namespace PhonebookFilesRepositories.Views
{
    class ContactView
    {
        public void Show()
        {
            while (true)
            {
                ContactMenu choice = RenderMenu();

                try
                {
                    switch (choice)
                    {
                        case ContactMenu.Select:
                            {
                                GetAll();
                                break;
                            }
                        case ContactMenu.View:
                            {
                                View();
                                break;
                            }
                        case ContactMenu.Insert:
                            {
                                Add();
                                break;
                            }
                        case ContactMenu.Update:
                            {
                                Update();
                                break;
                            }
                        case ContactMenu.Delete:
                            {
                                Delete();
                                break;
                            }
                        case ContactMenu.Exit:
                            {
                                return;
                            }
                    }
                }
                catch (Exception ex)
                {
                    Console.Clear();
                    Console.WriteLine(ex.Message);
                    Console.ReadKey(true);
                }
            }
        }

        private ContactMenu RenderMenu()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("Contacts management:");
                Console.WriteLine("[G]et all Contacts");
                Console.WriteLine("[V]iew Contact");
                Console.WriteLine("[A]dd Contact");
                Console.WriteLine("[E]dit Contact");
                Console.WriteLine("[D]elete Contact");
                Console.WriteLine("E[x]it");

                string choice = Console.ReadLine();
                switch (choice.ToUpper())
                {
                    case "G":
                        {
                            return ContactMenu.Select;
                        }
                    case "V":
                        {
                            return ContactMenu.View;
                        }
                    case "A":
                        {
                            return ContactMenu.Insert;
                        }
                    case "E":
                        {
                            return ContactMenu.Update;
                        }
                    case "D":
                        {
                            return ContactMenu.Delete;
                        }
                    case "X":
                        {
                            return ContactMenu.Exit;
                        }
                    default:
                        {
                            Console.WriteLine("Invalid choice.");
                            Console.ReadKey(true);
                            break;
                        }
                }
            }
        }

        private void GetAll()
        {
            Console.Clear();

            ContactRepository contactsRepository = new ContactRepository(@"../../DataFiles/contacts.txt");
            PhoneRepository phonesRepository = new PhoneRepository(@"../../DataFiles/phones.txt");
            List<Contact> contacts = contactsRepository.GetAll(AuthenticationService.LoggedUser.Id);

            foreach (Contact contact in contacts)
            {
                Console.WriteLine("ID:" + contact.Id);
                Console.WriteLine("Name :" + contact.FullName);
                Console.WriteLine("Email :" + contact.Email);

                List<Phone> phones = phonesRepository.GetAll(contact.Id);
                foreach (Phone phone in phones)
                {
                    Console.WriteLine("ID:" + phone.Id);
                    Console.WriteLine("Phone :" + phone.PhoneNumber);
                }
                Console.WriteLine("########################################");
            }

            Console.ReadKey(true);
        }

        private void View()
        {
            Console.Clear();

            Console.Write("Contact ID: ");
            int contactId = Convert.ToInt32(Console.ReadLine());

            ContactRepository contactsRepository = new ContactRepository(@"../../DataFiles/contacts.txt");
            PhoneRepository phonesRepository = new PhoneRepository(@"../../DataFiles/phones.txt");

            Contact contact = contactsRepository.GetById(contactId);
            if (contact == null)
            {
                Console.Clear();
                Console.WriteLine("Contact not found.");
                Console.ReadKey(true);
                return;
            }

            PhoneView phoneView = new PhoneView(contact);
            phoneView.Show();
        }

        private void Add()
        {
            Console.Clear();

            Contact contact = new Contact();
            contact.ParentUserId = AuthenticationService.LoggedUser.Id;

            Console.WriteLine("Add new Contact:");
            Console.Write("Full Name: ");
            contact.FullName = Console.ReadLine();
            Console.Write("Email: ");
            contact.Email = Console.ReadLine();

            ContactRepository contactsRepository = new ContactRepository(@"../../DataFiles/contacts.txt");
            contactsRepository.Save(contact);

            Console.WriteLine("Contact saved successfully.");
            Console.ReadKey(true);

            PhoneView phoneView = new PhoneView(contact);
            phoneView.Show();
        }

        private void Update()
        {
            Console.Clear();

            Console.Write("Contact ID: ");
            int contactId = Convert.ToInt32(Console.ReadLine());

            ContactRepository contactsRepository = new ContactRepository(@"../../DataFiles/contacts.txt");
            Contact contact = contactsRepository.GetById(contactId);

            if (contact == null)
            {
                Console.Clear();
                Console.WriteLine("Contact not found.");
                Console.ReadKey(true);
                return;
            }

            Console.WriteLine("Editing Contact (" + contact.FullName + ")");
            Console.WriteLine("ID:" + contact.Id);

            Console.WriteLine("Name :" + contact.FullName);
            Console.Write("New Name:");
            string fullName = Console.ReadLine();
            Console.WriteLine("Email :" + contact.Email);
            Console.Write("New Email :");
            string email = Console.ReadLine();

            if (!string.IsNullOrEmpty(fullName))
                contact.FullName = fullName;
            if (!string.IsNullOrEmpty(email))
                contact.Email = email;

            contactsRepository.Save(contact);

            Console.WriteLine("Contact saved successfully.");
            Console.ReadKey(true);

            PhoneView phoneView = new PhoneView(contact);
            phoneView.Show();
        }

        private void Delete()
        {
            ContactRepository contactsRepository = new ContactRepository(@"../../DataFiles/contacts.txt");

            Console.Clear();

            Console.WriteLine("Delete Contact:");
            Console.Write("Contact Id: ");
            int contactId = Convert.ToInt32(Console.ReadLine());

            Contact contact = contactsRepository.GetById(contactId);
            if (contact == null)
            {
                Console.WriteLine("Contact not found!");
            }
            else
            {
                contactsRepository.Delete(contact);
                Console.WriteLine("Contact deleted successfully.");
            }
            Console.ReadKey(true);
        }
    }
}
