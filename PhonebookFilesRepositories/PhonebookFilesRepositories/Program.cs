﻿using PhonebookFilesRepositories.Services;
using PhonebookFilesRepositories.Views;

namespace PhonebookFilesRepositories
{
    class Program
    {
        static void Main(string[] args)
        {
            LoginView loginView = new LoginView();
            loginView.Show();

            if (AuthenticationService.LoggedUser.IsAdmin)
            {
                AdminView adminView = new AdminView();
                adminView.Show();
            }
            else
            {
                ContactView contactView = new ContactView();
                contactView.Show();
            }
        }
    }
}
